<?php
error_reporting(E_ERROR | E_PARSE);

use GuzzleHttp\Client;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require 'vendor/autoload.php';

$scraper = new Scraper();
$scraper->process();

class Scraper
{
    const OPENEXCHANGERATES_URI = 'https://openexchangerates.org/api/latest.json?app_id=ecf4b3bd2daf4f218bffddf78cfe84cc&show_alternative=true&base=btc';
    const USER_PAGE = 'accounts/profile/';
    const BASE_LB_URI = 'https://localbitcoins.com/';
    const OFFERS_SELL_URL = 'sell-bitcoins-online/.json';
    const OFFERS_BUY_URL = 'buy-bitcoins-online/.json';

    /** @var Client */
    private $client;

    /** @var mixed  */
    private $btcMarketPrice;

    /** @var Worksheet */
    private $spreadSheet;

    /** @var int  */
    private $processed = 0;

    /** @var array  */
    private $cacheUserNames = [];

    /** @var  */
    private $log;

    public function __construct()
    {
        $this->client = new Client();
        $this->btcMarketPrice = $this->getBtcMarketPrice();
        $this->spreadSheet = $this->createSpreadSheets();
        $this->log = fopen('logs.txt', 'a') or die('Unable to open file!');
    }


    public function process()
    {
        try {
            $start = microtime(true);

            //Offers
            $this->scrapeOffers(self::BASE_LB_URI . self::OFFERS_SELL_URL);
            $this->scrapeOffers(self::BASE_LB_URI . self::OFFERS_BUY_URL);
            $this->saveSpreadSheet();

            //Users
            $this->scrapeUsers();
            $this->saveSpreadSheet();

            $end = microtime(true);

            fwrite($this->log, "\n". 'Done!');
            fwrite($this->log, "\n". 'Script worked for ' . ($end - $start) . ' seconds.');
            fclose($this->log);
        } catch (\Exception $e) {
            fwrite($this->log, "\n".$e->getMessage());
        }
    }

    /**
     * @param string $uri
     * @return string|bool
     * @throws HttpResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function scrapeOffers(string $uri)
    {
        if ($uri) {
            $time_start = microtime(true);
            $response = $this->client->request('GET', $uri);
            if ($response->getStatusCode() === 200) {
                $content = json_decode($response->getBody(), true);
                $offers = $content['data']['ad_list'];
                $offerToSave = [];
                foreach ($offers as $offerData) {
                    $offerToSave[] = (array)$this->createOfferFromData($offerData);
                }
                $this->processOffers($offerToSave);

                $this->processed += $content['data']['ad_count'];
                $time_end = microtime(true);
                fwrite($this->log, "\n". 'Processed ' . $content['data']['ad_count'] . ' offers for ' . ($time_end - $time_start) . ' sec. Totally '.$this->processed."\n");

                if ($nextUri = $content['pagination']['next'] ?? false) {
                    fwrite($this->log, "\n". 'Next '. $content['pagination']['next']);
                    $this->scrapeOffers($nextUri);
                }
            } else {
                throw new \HttpResponseException('Error response from LocalBitcoins: '
                    . $response->getStatusCode() . ': ' . $response->getBody());
            }
        }
    }


    /**
     * @throws HttpResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function scrapeUsers()
    {
        if (!empty($this->cacheUserNames)) {
            fwrite($this->log, "\n". 'Cached '.count($this->cacheUserNames).' users.');
            $i = 0;
            $usersToSave = [];
            foreach ($this->cacheUserNames as $userName) {
                $time_start = microtime(true);
                try {
                    //scraping info from website
                    $userProfileUri = self::BASE_LB_URI . self::USER_PAGE . $userName . '/';
                    $dom = new DomDocument();
                    $dom->loadHTMLFile($userProfileUri);
                    $xpath = new DomXPath($dom);
                    $bio = '';
                    foreach ($xpath->query("//div[@class='overflow-catch']/p") as $n){
                        $bio .= $n->nodeValue;
                    }

                    $tradesCount = $xpath->query("//div[@id='confirmed_trades_row']/div/strong[@class='F100_score']");
                    $btcVolume = $xpath->query("//div[@id='trade_volume_row']/div[@class='col-md-6 profile-value']");
                    $language = $xpath->query("//div[@id='trusted_row']/div[@class='col-md-6 profile-value']");

                    //scraping feedbacks
                    $userFeedbackUri = self::BASE_LB_URI . self::USER_PAGE . '/' . $userName . '/feedback/';
                    $dom->loadHTMLFile($userFeedbackUri);
                    $xpath = new DomXPath($dom);
                    $feedback = [];
                    foreach ($xpath->query("//div[@class='row']/div/a") as $link) {
                        $parts = explode(':', $link->nodeValue);
                        $feedback[trim($parts[0])] = trim($parts[1]);
                    }

                    $usersToSave[] = [
                        'username' => $userName,
                        'bio' => preg_replace('/[^\p{L}\d\.\(\)\+\%\!\"\s\,\:]/u', '', $bio),
                        'link' => $userProfileUri,
                        'feedbackTotal' => $feedback['All'],
                        'feedbackNegative' => $feedback['Negative'],
                        'feedbackNeutral' => $feedback['Neutral'],
                        'feedbackPositive' => $feedback['Positive'],
                        'tradesTotal' => trim($tradesCount[0]->nodeValue),
                        'tradePartners' => trim($tradesCount[1]->nodeValue),
                        'btcVolume' => trim($btcVolume[0]->nodeValue),
                        'language' => trim($language[0]->nodeValue),
                    ];
                } catch (\Throwable $e) {
                    fwrite($this->log, "\n".'User '.$userName.' did not writed because of '.$e->getMessage());
                }

                $time_end = microtime(true);
                fwrite($this->log, "\n".  'Processed ' . $i . ' users for ' . ($time_end - $time_start) . " sec. \n");
                $time_start = microtime(true);
                $i++;
            }
            $this->processUsers($usersToSave);
        }
    }


    /**
     * Now just save to excel file
     * @param array $offers
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function processOffers(array $offers)
    {
        $sheet = $this->spreadSheet->getSheetByName('Offers');
        $sheet->fromArray($offers, '', 'A' . ($sheet->getHighestDataRow('A') + 1));
    }


    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function createSpreadSheets(): Spreadsheet
    {
        $spreadSheet = new Spreadsheet();
        $offersSheet = $spreadSheet->getActiveSheet();
        $offersSheet->setTitle('Offers');
        $offersSheet->setCellValue('A1', 'Username');
        $offersSheet->setCellValue('B1', 'Markup');
        $offersSheet->setCellValue('C1', 'Type');
        $offersSheet->setCellValue('D1', 'PaymentMethod');
        $offersSheet->setCellValue('E1', 'PaymentMethodDescription');
        $offersSheet->setCellValue('F1', 'Currency');
        $offersSheet->setCellValue('G1', 'USD min');
        $offersSheet->setCellValue('H1', 'USD max');
        $offersSheet->setCellValue('I1', 'Link');
        $offersSheet->setCellValue('J1', 'Terms');

        foreach(range('A','J') as $columnID) {
            $offersSheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $usersSheet = $spreadSheet->createSheet();
        $usersSheet->setTitle('Users');
        $usersSheet->setCellValue('A1', 'Username');
        $usersSheet->setCellValue('B1', 'BIO');
        $usersSheet->setCellValue('C1', 'Link');
        $usersSheet->setCellValue('D1', 'Feedback total');
        $usersSheet->setCellValue('E1', 'Feedback negative');
        $usersSheet->setCellValue('F1', 'Feedback neutral');
        $usersSheet->setCellValue('G1', 'Feedback positive');
        $usersSheet->setCellValue('H1', 'Total trades');
        $usersSheet->setCellValue('I1', 'Total trade partners');
        $usersSheet->setCellValue('J1', 'Btc volume');
        $usersSheet->setCellValue('K1', 'Language(s)');

        foreach(range('A','K') as $columnID) {
            $usersSheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        return $spreadSheet;
    }


    /**
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    private function saveSpreadSheet()
    {
        $writer = new Xlsx($this->spreadSheet);
        $writer->save('output/localBitcoins_'.date('Ymd_H-i').'.xlsx');
    }


    /**
     * @param array $offerData
     * @return Offer
     * @throws Exception
     */
    private function createOfferFromData(array $offerData): Offer
    {
        if (!$this->btcMarketPrice) {
            throw new \LogicException('Error getting BTC market price');
        }
        $markup = (float)$offerData['data']['temp_price_usd']*100/$this->btcMarketPrice;
        $usdPrice = (float)$offerData['data']['temp_price']/(float)$offerData['data']['temp_price_usd'];
        $usdMin = $offerData['data']['min_amount']/$usdPrice;
        $usdMax = $offerData['data']['max_amount']/$usdPrice;

        //Cache userNames
        if (!in_array($offerData['data']['profile']['username'], $this->cacheUserNames, true)) {
            $this->cacheUserNames[] = $offerData['data']['profile']['username'];
        }

        $offer = new Offer();
        $offer->markup = round($markup) . '%';
        $offer->username = $offerData['data']['profile']['username'];
        $offer->terms = preg_replace('/[^\p{L}\d\.\(\)\+\%\!\"\s\,\:]/u', '', $offerData['data']['msg']);
        $offer->paymentMethod = $offerData['data']['online_provider'];
        $offer->paymentMethodDescription = preg_replace('/[^\p{L}\d\.\(\)\+\%\!\"\s\,\:]/u', '', $offerData['data']['bank_name']);
        $offer->offerType = $offerData['data']['trade_type'];
        $offer->currency = $offerData['data']['currency'];
        $offer->usdMax = $usdMax;
        $offer->usdMin = $usdMin;
        $offer->link = $offerData['actions']['public_view'];

        return $offer;
    }


    /**
     * @param array $users
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function processUsers(array $users)
    {
        $sheet = $this->spreadSheet->getSheetByName('Users');
        $sheet->fromArray($users, '', 'A' . ($sheet->getHighestDataRow('A') + 1));
    }


    /**
     * @param string $currency
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getBtcMarketPrice($currency = 'USD')
    {
        $response = $this->client->request('GET', self::OPENEXCHANGERATES_URI);
        if ($response->getStatusCode() === 200) {
            $content = json_decode($response->getBody(), true);
            if (array_key_exists($currency, $content['rates'])) {
                return $content['rates'][$currency];
            }
        }

        return false;
    }
}


class Offer
{
    public $username;

    public $markup;

    public $offerType;

    public $paymentMethod;

    public $paymentMethodDescription;

    public $currency;

    public $usdMin;

    public $usdMax;

    public $link;

    public $terms;
}
